import sys
import lib_process


def main():
    """ main
    """

    if len(sys.argv) < 4:
        print("Usage {} source_folder file_extension destination_folder".format(
            sys.argv[0]))
        return 1

    search_folder = sys.argv[1]
    search_pattern = sys.argv[2]
    destination_folder = sys.argv[3]

    found = lib_process.get_files(search_folder, search_pattern)
    lib_process.move_files(found, destination_folder)

if __name__ == '__main__':
    main()
