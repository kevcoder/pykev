import os
import shutil
import time
import datetime


def get_files(folder_path, pattern):
    """
    will return all files matching pattern in the folder_path
    args:
        folder_path (str): the folder to search
        pattern (str): a regex pattern used to filter files
    """
    files = []
    for (dirpath, _, filenames) in os.walk(folder_path):
        for filename in filenames:
            ext = (os.path.splitext(filename)[1]).lower()
            if ext in ['.wav', '.mpg', '.mp3'] + [pattern]:
                files.append(os.path.join(dirpath, filename))

    return files


def move_files(files, destnication_folder):
    """
    move the array of files into the destnication_folder

    Args:
        files (array): array of string of full path
        destnication_folder (str): the root folder to copy the files into
    """
    subfolder = time.strftime("%Y%m%d_%H%M%S")
    newfolder = os.path.join(destnication_folder, subfolder)
    if not os.path.isdir(newfolder):
        os.makedirs(newfolder)
    for filename in files:
        shutil.move(filename, newfolder)
